This repo is for basic interview preparation.
1. It includes basic TestNG framework. Search for GojekTests file to understand basic flow of test.java.tests.tests in TestNG.
2. It has Log4J support to save logs into console and file.
3. Has http utils to make http calls using httpclient.
4. Has JsonUtils to validate JSONSchema and also validate two JSONs are equal.
