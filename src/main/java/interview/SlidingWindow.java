package main.java.interview;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SlidingWindow {

    public static void main(String[] args) {
        int[] arr = {1, 5, 2, 3, 7, 1};
        int k=3;
        System.out.println("max sum is : " + maxSumofKConsecutiveElements(arr, k));


        String inputStr = "pwwkew";
        /*System.out.println("length of the longest substring without repeating characters is : " +
                lengthOfLongestSubstrWithoutRepeat(inputStr));*/

    }


    /*Given an array of integers and a number k.
Return the highest sum of any k consecutive elements in the array.
Examples:
Input: arr = [1, 5, 2, 3, 7, 1], k = 3
Output: 12 (the sum of subarray [2, 3, 7])
Input: arr = [5, -3, 7, -6, 8], k = 2
Output: 4 (the sum of subarray [-3, 7])
Input: arr = [5, -3, 7, -6, 8], k = 3
Output: 9 (the sum of subarray [5, -3, 7] or [7, -6, 8])*/
    public static int maxSumofKConsecutiveElements(int[] arr, int k) {
        int curSum = sumOfKElements(arr, 0, k);

        int maxSum = curSum;
        for (int i=0; i<arr.length-k;i++) {
            curSum = curSum - arr[i] + arr[i+k];
            maxSum = Math.max(maxSum, curSum);
        }
        return maxSum;
    }

    private static int sumOfKElements(int[] arr, int startIndex,int endIndex) {
        int curSum = 0;
        for (int i=startIndex; i<endIndex;i++) {
            curSum += arr[i];
        }
        return curSum;
    }


    /*Given a string, find the length of the longest substring without repeating characters.
Full description at https://leetcode.com/problems/longest-substring-without-repeating-characters/
Examples:
Input: "pwwkew"
Output: 3 (the substring "wke" or "kew")
Input: "abcabcbb"
Output: 3 (the substring "abc" or "bca" or "cab")
Input: "bbbbb"
Output: 1 (the substring "b")*/
    public static int lengthOfLongestSubstrWithoutRepeat(String inputStr) {
        char[] chars = inputStr.toCharArray();
        Set<Character> characterSet = new HashSet<>();
        int right =1;
        int cur=0;
        int maxLength = 1;

        Map<Character, Integer> map = new HashMap<>();
        map.put(chars[cur], 1);

        while (right<chars.length) {
            map.put(chars[right], map.get(chars[right]) + 1);

            if (map.get(chars[right])>1) {
                map.put(chars[right], map.get(chars[right]) - 1);

            } else {
            }
            right++;


        }
        return 0;
    }


}
