package main.java.interview;

public class ProductArray {
    public static void main(String[] args) {
        int[] arr = {2,2,3, 5}; // output : 30, 30, 20, 12
        int[] finalarr = productArray(arr);
        for (int i=0; i<finalarr.length; i++) {
            System.out.println(finalarr[i]);
        }
    }
    /*1 2 3


     * 6 3 2*/
    static int[] productArray(int arr[]) {
        int curIndex = 0;

        //range : 0-10
        // i=1 :
        int[] finalArr = new int[10];
        int k=0;
        for (int i=0; i<arr.length;i++) {
            int temp1 = i - 1;
            int temp2 = i + 1;
            int leftProduct = 1;
            int rightProduct = 1;
            //left product
            while (temp1>=0) {
                leftProduct *= arr[temp1];
                temp1 --;
            }
            //right product
            while (temp2<arr.length) {
                rightProduct *= arr[temp2];
                temp2++;
            }
            //multiply both
            finalArr[k++] = leftProduct * rightProduct;
        }
        return finalArr;
    }
}
