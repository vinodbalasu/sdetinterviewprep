package main.java.interview.dp.knapsack;

public class MinimumSubsetSumDiff {
    public static void main(String[] args) {
        int[] arr = {1,3,5,7};//expect 0 as the response
        /*It is more of derivative to Equal Sum partition*/
        /*Given an array. Minimize the difference of subset sum and return it.
        * |S2-S1|= Minimum value
        * S2 + S1 = Range(Array Sum)
        * S2-S1 = Range - 2S1
        * Min(Range - 2S1) value will be the minimum subset sum difference.
        * Ideally, The subset sum problem table whose last row has the details of sums possible for the given array.
        * Identify the true values of the last row and calculate the Minimum of |Range -2S1| and return it. Here S1 will
        * take values till half of the range(thats a consideration we are making)
        * */
        System.out.println(getMinimumSubsetSumDiff(arr, arr.length));
    }

    static int getMinimumSubsetSumDiff(int[] wt, int n) {
        //get the range or arraysum
        int range = EqualSumPartition.getArraySum(wt);

        //calculate subset table for range
        boolean[][] t = getSubsetSumValues(wt, range, wt.length);
        int[] sums = new int[range];
        int i=0;
        //get the last row and filter out the subset sum possible cells for different weights or sums.
        for (int j=0; j<t[n].length; j++) {
            if (t[n][j])
                sums[i++] = j;
        }

        //for S1 we can just use half of sums array elements to calculate the minimum sum diff
        int min = Integer.MAX_VALUE;
        for (int k=0; k<sums.length/2; k++) {
            min = Math.min(min, range-2*sums[k]);
        }
        return min;
    }

    static boolean[][] getSubsetSumValues(int[] wt, int w, int n) {
        boolean[][] dp = new boolean[n+1][w+1];
        //initialization
        for (int i=0; i<n+1; i++) {
            for (int j=0; j<w+1; j++) {
                if (i==0) {
                    dp[i][j] = false;
                }

                if (j==0) {
                    dp[i][j] = true;
                }
            }
        }

        for (int i=1; i<n+1; i++) {
            for (int j=1; j<w+1; j++) {
                if (wt[i-1] <= j)
                    dp[i][j] = dp[i-1][j-wt[i-1]] || dp[i-1][j];
                else
                    dp[i][j] = dp[i-1][j];
            }
        }
        return dp;
    }
}
