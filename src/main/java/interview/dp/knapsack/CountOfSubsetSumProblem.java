package main.java.interview.dp.knapsack;

public class CountOfSubsetSumProblem {
    public static void main(String[] args) {
        //get the number of subsets present in the array whose sum is 10.
        int[] arr = {2,3,5,6,8,10};
        int sum = 10;
        //number of subsets here is 3 ( {2,3,5}, {2,8}, {10})
        System.out.println("count of subset sums is : " + getCountofSubsetSum(arr, sum, arr.length));
    }

    /*Initialisation :
    * empty subsets are possible in case if sum is 0. So all the elements in having j==0 will have value as 1.
    * 1 0 0 0 0 0 0 0 0 0
    * 1
    * 1
    * 1
    * 1
    * 1
    *  */
    static int getCountofSubsetSum(int[] wt, int w, int n) {
        int[][] t = new int[n+1][w+1];
        //initialisation
        for (int i=0; i<n+1; i++) {
            for (int j=0; j<w+1; j++) {
                if (i==0)
                    t[i][j] = 0;
                if (j==0)
                    t[i][j] = 1;
            }
        }
        //choice diagram
        for (int i=1; i<n+1; i++) {
            for (int j=1; j<w+1; j++) {
                if (wt[i-1] <= j)
                    t[i][j] = t[i-1][j-wt[i-1]] + t[i-1][j];
                else
                    t[i][j] = t[i-1][j];
            }
        }
        return t[n][w];
    }
}
