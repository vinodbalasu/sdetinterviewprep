package main.java.interview.dp.knapsack;

public class _01KnapsackProblem {
    static int[][] t = new int[1000][1000];
    public static void main(String[] args) {
        //given items -> wt[1,3,4,5], val[1,4,5,7]
        //max weight bag can store is : 7
        //fetch the maxVal or maxProfit
        int[] wt = {1,3,4,5};
        int[] val = {1,4,5,7};
        int weight = 7;
        System.out.println("Max profit is : " + knapsackRecursive(wt, val, weight, wt.length));
        intialiseTArray(t);
        System.out.println("Max profit using memoization is : " + knapsackRecursiveMemoization(wt, val, weight, wt.length));

        System.out.println("Max profit using tabulation is : " + knapsackTabulationApproach(wt, val, weight, wt.length));

    }

    static void intialiseTArray(int[][] t) {
        for (int i=0; i<t.length; i++)
            for (int j=0; j<t[0].length; j++)
                t[i][j] = -1;
    }

    /*Reference : https://www.youtube.com/watch?v=F7wqWbqYn9g&list=PL_z_8CaSLPWekqhdCPmFohncHwz8TY2Go&index=9*/
    static int knapsackRecursive(int[] wt, int[] val, int weight, int n) {
        if (weight==0 || n ==0)
            return 0;
        if (wt[n-1] <= weight) {
            return Math.max(val[n-1] + knapsackRecursive(wt, val, weight - wt[n-1], n-1),
                    knapsackRecursive(wt, val, weight, n-1));
        } else if (wt[n-1] > weight) {
            return knapsackRecursive(wt, val, weight, n-1);
        }
        return -1;
    }

    //some recursive calls which do repeat computation are skipped because of this.
    static int knapsackRecursiveMemoization(int[] wt, int[] val, int w, int n) {
        if (w==0 || n ==0)
            return 0;
        if (t[n][w] != -1)
            return t[n][w];
        if (wt[n-1] <= w) {
            t[n][w] = Math.max(val[n-1] + knapsackRecursive(wt, val, w - wt[n-1], n-1),
                    knapsackRecursive(wt, val, w, n-1));
        } else if (wt[n-1] > w) {
            t[n][w] = knapsackRecursive(wt, val, w, n-1);
        }

        return t[n][w];
    }


    static int knapsackTabulationApproach(int[] wt, int[] val, int w, int n) {
        int[][] t = new int[n+1][w+1];

        for (int i=0; i<n+1; i++) {
            for (int j=0; j<w+1; j++) {
                if (i==0 || j==0)
                    t[i][j] = 0;
            }
        }

        for (int i=1; i<n+1; i++) {
            for (int j=1; j<w+1; j++) {
                if (wt[i-1] <= j) {
                    t[i][j] = Math.max(val[i-1] + t[i-1][j-wt[i-1]], t[i-1][j]);
                } else if (wt[i-1] > j) {
                    t[i][j] = t[i-1][j];
                }
            }
        }

        return t[n][w];

    }

}
