package main.java.interview.dp.knapsack;

public class EqualSumPartition {
    public static void main(String[] args) {
        /*Given array. return true when array is divided into two subsets and has equal sum. check if equal partition
        is possible or not*/
        int[] arr = {1,5,11,5};
        System.out.println("is Equal Sum partition possible : " + equalSumPartitionPossible(arr));
    }

    /*Approach : If the arraySum is even -> it can be divided into two equal partitions. Else return false because two
    * equal paritions is not possible*/
    static boolean equalSumPartitionPossible(int[] arr) {
        int sum = getArraySum(arr);

        if (sum%2!=0) {
            //if sum is odd
            return false;
        }
        //if sum is even. check if subset is present whose weight is sum/2
        int w = sum/2;
        return SubsetSumProblem.subsetSumPresent(arr, w, arr.length);
    }

    static int getArraySum(int[] arr) {
        int sum=0;
        for (int i=0; i<arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }
}
