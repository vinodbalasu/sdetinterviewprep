package main.java.interview.dp.knapsack;

public class SubsetSumProblem {
    public static void main(String[] args) {
        /*given i/p array  and sum, Find if there is a subset whose value is equal to sum*/
        int[] arr = {2,3,7,8,10};
        int sum = 11;
        System.out.println("subset sum Presence value is : " + subsetSumPresent(arr, sum, arr.length));
    }

    static boolean subsetSumPresent(int[] wt, int w, int n) {
        boolean[][] t = new boolean[n+1][w+1];
        //initialisation
        //if array is empty or has n elements and weight=0, then empty subset is possible
        //if array is empty and sum = 1...w, then no subset is possible
        for (int i=0; i<n+1; i++) {
            for (int j=0; j<w+1; j++) {
                if (i==0)
                    t[i][j] = false;
                if (j==0)
                    t[i][j] = true;
            }
        }

        //choice diagram implementation
        for (int i=1; i<n+1; i++) {
            for (int j=1; j<w+1; j++) {
                if (wt[i-1] <= j) {
                    t[i][j] = t[i-1][j-wt[i-1]] || t[i-1][j];
                } else {
                    t[i][j] = t[i-1][j];
                }
            }
        }
        return t[n][w];
    }
}
