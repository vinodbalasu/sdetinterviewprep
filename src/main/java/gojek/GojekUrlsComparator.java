package main.java.gojek;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/*Your code should read the (HTTP/HTTPS) requests from File 1 and File 2, use any http client library, and
compare the response of
File 1 : line 1 & File 2 : line 1,
File 1 : line 2 & File 2 : line 2,
File 1 : line 3 & File 2 : line 3,
....and so on

Must have

● Your compare function should be able to compare two JSON responses. [Avoid using a third party
library or dependency for comparing JSONs]
● Assume that your code should be capable of comparing millions of API requests without any
memory issues. [Mandatory: Test with at least 1000 requests]
● Handle exceptions gracefully (don't terminate the program in case of exceptions)
● Print the output when responses are compared in the below format.

https://reqres.in/api/users/3 not equals https://reqres.in/api/users/1
https://reqres.in/api/users/2 equals https://reqres.in/api/users/2
https://reqres.in/api/users?page=1 equals https://reqres.in/api/users?page=1*/
public class GojekUrlsComparator {
    public static final String LHS_URLS_PATH = "src/test/resources/testdata/gojek/requests_lhs.txt";
    public static final String RHS_URLS_PATH = "src/test/resources/testdata/gojek/requests_rhs.txt";

    public static void main(String[] args) {
        final GojekComparator gojekComparator = GojekComparatorFactory.getInstance("json");
        CompareWrapper compareWrapper = null;
        try {
            compareWrapper = gojekComparator.getData(new File(LHS_URLS_PATH), new File(RHS_URLS_PATH));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        List<String> lhsList = (List<String>) compareWrapper.getX();
        List<String> rhsList = (List<String>) compareWrapper.getY();
        final ExecutorService executorService = Executors.newFixedThreadPool(3);
        final List<UrlResponseCallable> urlResponseCallables = new ArrayList<>();
        for (int i=0, j=0; i<lhsList.size() && j < rhsList.size(); i++, j++) {
            final UrlResponseCallable urlResponseCallable = new UrlResponseCallable(lhsList.get(i), rhsList.get(i));
            urlResponseCallables.add(urlResponseCallable);
        }


        //Execute all tasks and get reference to Future objects
        List<Future<String>> resultList = null;
        try {
            resultList = executorService.invokeAll(urlResponseCallables);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        for (int i = 0; i < resultList.size(); i++) {
            Future<String> future = resultList.get(i);
            try {
                String result = future.get();
                System.out.println(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown();
    }
}
