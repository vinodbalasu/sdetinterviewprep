package main.java.gojek;

import lombok.Data;

@Data
public class CompareWrapper<X, Y> {
    X x;
    Y y;
}
