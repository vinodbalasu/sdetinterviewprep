package main.java.gojek;

import main.java.utils.commonutils.JsonUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JsonComparator implements GojekComparator {
    @Override
    public boolean compare(Object o1, Object o2) {
        return JsonUtils.areEqual(new JSONObject(o1), new JSONObject(o2));
    }

    @Override
    public CompareWrapper getData(File file1, File file2) throws FileNotFoundException {
        CompareWrapper<List<String>, List<String>> dataList = new CompareWrapper<>();
        Scanner scanner = new Scanner(file1);
        final List<String> lhsUrls = new ArrayList<>();
        while (scanner.hasNextLine())
            lhsUrls.add(scanner.nextLine());

        scanner = new Scanner(file2);
        final List<String> rhsUrls = new ArrayList<>();
        while (scanner.hasNextLine())
            rhsUrls.add(scanner.nextLine());

        dataList.setX(lhsUrls);
        dataList.setY(rhsUrls);
        return dataList;
    }
}
