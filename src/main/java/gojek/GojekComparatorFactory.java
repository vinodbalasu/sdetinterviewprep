package main.java.gojek;

public class GojekComparatorFactory {

    public static GojekComparator getInstance(String type) {
        switch (type) {
            case "json" :
                return new JsonComparator();
            default:
                break;
        }
        return null;
    }
}
