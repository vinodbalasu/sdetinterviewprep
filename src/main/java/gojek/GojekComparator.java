package main.java.gojek;

import java.io.File;
import java.io.FileNotFoundException;

public interface GojekComparator<X, Y> {
    public boolean compare(X x, Y y);
    public CompareWrapper<X, Y> getData(File file1, File file2) throws FileNotFoundException;
}
