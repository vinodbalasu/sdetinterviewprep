package main.java.gojek;

import main.java.utils.Log;
import main.java.utils.http.HttpClientWrapper;

import java.util.concurrent.Callable;

public class UrlResponseCallable implements Callable<String> {
    private final String lhsUrl;
    private final String rhsUrl;

    public UrlResponseCallable(String lhsUrl, String rhsUrl) {
        this.lhsUrl = lhsUrl;
        this.rhsUrl = rhsUrl;
    }

    @Override
    public String call() throws Exception {
        //need to do multithreading for the data obtained
        //make a get call and validate both the responses
        dto.http.Response responseLHS = null;
        try {
            responseLHS = HttpClientWrapper.sendGET(lhsUrl);
        } catch (Exception e) {
            e.printStackTrace();
            Log.info("Error in get call for url : " + lhsUrl);
        }

        dto.http.Response responseRHS = null;
        try {
            responseRHS = HttpClientWrapper.sendGET(rhsUrl);
        } catch (Exception e) {
            e.printStackTrace();
            Log.info("Error in get call for url : " + rhsUrl);
        }
        final String responseLHSStr = responseLHS != null ? responseLHS.getBody() : "{}";
        final String responseRHSStr = responseRHS != null ? responseRHS.getBody() : "{}";
        //add comparison logic here
        boolean areEqual = GojekComparatorFactory.getInstance("json").compare(responseLHSStr, responseRHSStr);
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(lhsUrl).append(areEqual ? " equals " : " not equals ").append(rhsUrl);
        return stringBuilder.toString();
    }
}
