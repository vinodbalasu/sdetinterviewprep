package main.java.utils.http;

import dto.http.Response;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import main.java.utils.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

public class HttpClientWrapper {

    public static Response sendGET(String url) throws Exception {
        final String curlRequest = "curl -X GET " + " " + url + " ";
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        final HttpGet httpGet = new HttpGet(url);
        Log.debug(curlRequest);
        final CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
        final StringBuffer body = new StringBuffer();

        if (httpResponse.getEntity() != null) {
            String inputLine;
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            while ((inputLine = reader.readLine()) != null) {
                body.append(inputLine);
            }
            reader.close();
        }
        httpClient.close();

        final HashMap<String, String> tempResponseHeaders = new HashMap<String, String>();

        Header[] responseHeaders = httpResponse.getAllHeaders();
        for (Header header : responseHeaders)
            tempResponseHeaders.put(header.getName(), header.getValue());

        final Response response = Response.builder()
                .body(body.toString())
                .statusCode(httpResponse.getStatusLine().getStatusCode())
                .statusLine(httpResponse.getStatusLine().toString())
                .headers(tempResponseHeaders)
                .build();

        Log.debug("Time: " + System.currentTimeMillis());
        Log.debug("GET status code: " + response.getStatusCode());
        Log.debug("Response body: " + response.getBody());
        return response;
    }

}
