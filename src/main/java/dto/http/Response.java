package dto.http;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    private String statusLine;
    private Integer statusCode;
    private String body;
    private HashMap<String,String> headers;
}
