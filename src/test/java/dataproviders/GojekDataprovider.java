package test.java.dataproviders;

import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GojekDataprovider {
    public static final String REQUEST_LHS_PATH = "src/test/resources/testdata/gojek/requests_lhs.txt";
    public static final String REQUEST_RHS_PATH = "src/test/resources/testdata/gojek/requests_rhs.txt";

    @DataProvider(name = "getFilesData", parallel = true)
    public Object[][] getFilesData() throws FileNotFoundException {
        //read line by line from both the request files and convert them into a single 2-d array
        final Scanner scanner1 = new Scanner(new File(REQUEST_LHS_PATH));
        final Scanner scanner2 = new Scanner(new File(REQUEST_RHS_PATH));
        final List<String> list1 = new ArrayList<String>();
        final List<String> list2 = new ArrayList<String>();

        while (scanner1.hasNextLine())
            list1.add(scanner1.nextLine());
        while (scanner2.hasNextLine())
            list2.add(scanner2.nextLine());
        int size = Math.max(list1.size(), list2.size());

        Object[][] data = new Object[size][2];
        for (int i=0; i<size; i++) {
            data[i] = new Object[]{list1.get(i), list2.get(i)};
        }
        return data;
    }
}
