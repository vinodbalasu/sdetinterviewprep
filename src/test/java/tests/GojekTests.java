package test.java.tests;

import dto.http.Response;
import main.java.Group;
import main.java.utils.commonutils.JsonUtils;
import main.java.utils.http.HttpClientWrapper;
import org.json.JSONObject;
import org.testng.annotations.Test;
import main.java.utils.Log;
import test.java.dataproviders.GojekDataprovider;


/* This is a dummy test where we have two files with requests. This test will fetch one url from file1 and other url from
   file 2 and the send a get call for both the urls and then compare the JSON responses and print the Output.*/
public class GojekTests {

    @Test(groups = {Group.GOJEK_FILE_TEST}, dataProviderClass = GojekDataprovider.class, dataProvider = "getFilesData")
    public void getFilesAndCompare(final String request_lhs, final String request_rhs) {

        //make a get call and validate both the responses
        Response responseLHS = null;
        try {
            responseLHS = HttpClientWrapper.sendGET(request_lhs);
        } catch (Exception e) {
            e.printStackTrace();
            Log.info("Error in get call for url : " + request_lhs);
        }

        Response responseRHS = null;
        try {
            responseRHS = HttpClientWrapper.sendGET(request_rhs);
        } catch (Exception e) {
            e.printStackTrace();
            Log.info("Error in get call for url : " + request_rhs);
        }

        final String responseLHSStr = responseLHS != null ? responseLHS.getBody() : "{}";
        final String responseRHSStr = responseRHS != null ? responseRHS.getBody() : "{}";
        //add comparison logic here
        final boolean areEqual = JsonUtils.areEqual(new JSONObject(responseLHSStr), new JSONObject(responseRHSStr));
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(request_lhs).append(areEqual ? " equals " : " not equals ").append(request_rhs);
        Log.debug(stringBuilder.toString());
    }
}
